document.addEventListener("DOMContentLoaded", function() {
    var form = document.getElementById("cta-form");
    var emailInput = document.getElementById("cta-email");
    var submitButton = document.getElementById("cta-submit");
    form.addEventListener("submit", function(event) {
        submitButton.disabled = true;
        submitButton.value = "..."

        var email = emailInput.value;
        fetch("https://app.microspoolers.dev/push-to-mailing-list", {
            method: "POST",
            mode: "cors",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body: "email=" + email,
        })
            .then(function() {
                submitButton.classList.add("finished")
                submitButton.value = "🎉"
            })
        event.preventDefault();
    })
});